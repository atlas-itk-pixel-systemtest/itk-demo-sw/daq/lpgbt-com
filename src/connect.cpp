#include <iostream>

#include "connect.hpp"


std::shared_ptr<spdlog::logger>
make_log(std::string logname) {
  auto sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  std::string log_pattern = "[%T:%e]%^[%=12l][%=15n]:%$ %v";
  auto logger = spdlog::get(logname);
  if(logger) return logger;
  logger = std::make_shared<spdlog::logger>(logname);
  spdlog::register_logger(logger);

  logger->sinks().push_back(sink);
  logger->set_level(spdlog::level::debug);
  logger->set_pattern(log_pattern);

  return logger;
}


std::map<netio_tag_t, NetioCtrl*> netio_clients;

// Netio Communication
void NetioCtrl::connect() {
  logger->info("Netio connect | host: {}:{} | tx.tag: 0x{:x}", remotehost, tx.port, tx.tag);

  ctx = new netio::context("posix");
  ctx_thread = std::thread([&](){ ctx->event_loop()->run_forever(); });

  tx.sock = new netio::low_latency_send_socket(ctx);
  tx.ep = netio::endpoint(remotehost.c_str(), tx.port);
  tx.sock->connect(tx.ep);

  if (!tx.sock->is_open()) {
    logger->error("Unable to connect to FELIX endpoint");
    throw std::runtime_error("ICMaster: Unable to connect to FELIX endpoint.");
    abort();
  } else {
    logger->debug("Connected to FELIX endpoint");
  }

  netio::low_latency_recv_socket::callback_fn cb_msg_recv =
    std::bind(&NetioCtrl::on_msg_recv, this, std::placeholders::_1, std::placeholders::_2);

  rx.sock = new netio::low_latency_subscribe_socket(ctx, cb_msg_recv);
  rx.ep = netio::endpoint(remotehost.c_str(), rx.port);

  netio_clients[rx.tag] = this;
  rx.sock->subscribe(rx.tag, rx.ep);
  logger->info("  subscribed  | host: {}:{} | rx.tag: 0x{:x}", remotehost, rx.port, rx.tag);
}

void NetioCtrl::disconnect() {
  logger->info("Netio | disconnect");
  if (tx.sock) tx.sock->disconnect();

  if (rx.sock) {
    rx.sock->unsubscribe(rx.tag, rx.ep);
    netio_clients.erase(rx.tag);
  }
  if (ctx) ctx->event_loop()->stop();
  if (ctx_thread.joinable()) ctx_thread.join();
  if (ctx) delete ctx;
}

void NetioCtrl::send_data(std::vector<uint8_t> &data) {
  // send to FELIX
  ToFlxHeader header;
  header.length = data.size();
  header.reserved = 0;
  header.elink = tx.tag;

  netio::message msg;

  msg.add_fragment((uint8_t*)&header, sizeof(header));
  msg.add_fragment(data.data(), data.size());

  flag = true;
  reply.clear();
  tx.sock->send(msg);
}

void NetioCtrl::on_msg_recv(netio::endpoint& ep, netio::message& msg) {
  logger->debug("Received data from {}:{} size: {}", ep.address(), ep.port(), msg.size());

  if (!flag) {
    logger->warn("send flag is not set, ignoring message.");
    return;
  }

  reply = msg.data_copy();

  uint16_t header_size = sizeof(felix::base::FromFELIXHeader);
  if (reply.size() < header_size) {
    reply.clear();
    logger->warn("Too small Netio message.");
    return;
  }

  // auto &header = *(reinterpret_cast<felix::base::FromFELIXHeader*>(reply.data()));
  // uint32_t tag = header.gbtid*0x40 + header.elinkid;
  // auto search = netio_clients.find(tag);
  // if (search == netio_clients.end()) {
  //   printf("unknown tag = 0x%x, ignoring message.\n", tag);
  //   reply.clear();
  //   return;
  // }
  // auto &client = *search->second;

  reply.erase(reply.begin(), reply.begin() + header_size);
  logger->trace("reply size: {}", reply.size());
  flag = false;
}



// NetioNext
void NetioNextCtrl::connect() {

  netio_init(&tx.ctx);

  tx.ctx.evloop.data = this;
  tx.ctx.evloop.cb_init = tx_on_init;

  std::cout << tx.host << " : " << tx.port << std::endl;

  std::cout << &tx.ctx << std::endl;
  std::cout << &tx.ctx.evloop << std::endl;

  tx.thread = std::thread([&](){netio_run(&tx.ctx.evloop);});

  netio_init(&rx.ctx);
  rx.ctx.evloop.data = this;
  std::cout << &rx.ctx.evloop << std::endl;
  rx.ctx.evloop.cb_init = rx_on_init;

  rx.thread = std::thread([&](){netio_run(&rx.ctx.evloop);});
}

void NetioNextCtrl::disconnect() {
  netio_unsubscribe(&rx.socket, rx.tag);
  netio_terminate(&rx.ctx.evloop);
  netio_terminate(&tx.ctx.evloop);
  rx.thread.join();
  tx.thread.join();
}

void NetioNextCtrl::send_data(std::vector<uint8_t> &data) {
  // send to FELIX
  ToFlxHeader header;
  header.length = data.size();
  header.reserved = 0;
  header.elink = tx.tag;

  std::vector<uint8_t> msg;
  msg.assign((uint8_t*)&header, (uint8_t*)&header + sizeof(header));
  msg.insert(msg.end(), data.begin(), data.end());

  // struct iovec iov[2];
  // iov[0].iov_base = (uint8_t*)&header;
  // iov[0].iov_len  = sizeof(header);

  // iov[1].iov_base = buff.data();
  // iov[1].iov_len  = buff.size();
   // auto res = netio_buffered_sendv(&tx.socket, iov, 2);
  flag = true;
  reply.clear();

  auto res = netio_buffered_send(&tx.socket, msg.data(), msg.size());
  netio_buffered_flush(&tx.socket);
  // NETIO_STATUS_TOO_BIG, NETIO_STATUS_AGAIN, NETIO_STATUS_OK
}

void NetioNextCtrl::tx_init() {
  logger->debug("NetioNext | tx_init");
  netio_buffered_socket_attr attr;

  // selected for IC e-link
  attr.num_pages = 256;
  attr.pagesize  = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms = 1000;

  netio_buffered_send_socket_init(&tx.socket, &tx.ctx, &attr);
  netio_buffered_connect(&tx.socket, tx.host.c_str(), tx.port);
  tx.socket.cb_connection_established   = tx_connection_established;
  tx.socket.cb_connection_closed        = tx_connection_closed;
  tx.socket.cb_error_connection_refused = tx_error_connection_refused;
  tx.socket.usr = this;
}

// Callbacks
void NetioNextCtrl::on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size) {
  //printf("received msg tag = 0x%03x size=%d\n", tag, size);

  // reply from felix-star ROM register lpGBT_v1
  // no felix header, status byte added
  // 00 - status - new for felix-star
  // e9 - 0x74 << 1 | 0x1 - read
  // 01 - command = 1
  // 01 - mem_size = 0x0001
  // 00
  // d7 - mem_addr = 0x01d7
  // 01
  // a6 - byte #1
  // 99 - parity

  auto &client = *static_cast<NetioNextCtrl*>(socket->usr);

  uint8_t* ptr = (uint8_t*)data;
  client.reply.assign(ptr + 1, ptr + size);
  client.flag = false;
}

void NetioNextCtrl::rx_init() {
  logger->debug("NetioNext | rx_init");

  struct netio_buffered_socket_attr attr;
  attr.num_pages = 256;
  attr.pagesize  = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms = 1000;

  logger->debug("Opening libfabric subscribe socket from {} on {}:{}",
                rx.localhost.c_str(), rx.host.c_str(), rx.port);
  netio_subscribe_socket_init(&rx.socket, &rx.ctx, &attr,
    rx.localhost.c_str(), rx.host.c_str(), rx.port);

  rx.socket.cb_connection_closed        = rx_connection_closed;
  rx.socket.cb_connection_established   = rx_connection_established;
  rx.socket.cb_error_connection_refused = rx_error_connection_refused;
  rx.socket.cb_msg_received             = on_msg_received;
  rx.socket.usr = this;

  logger->debug("attempting to subscribe to remote on {}:{}",
                rx.socket.remote_hostname, rx.socket.remote_port);
  logger->debug("subscribing to tag 0x{x}", rx.tag);
  netio_subscribe(&rx.socket, rx.tag);
}

