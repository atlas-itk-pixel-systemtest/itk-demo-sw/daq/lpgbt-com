#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
// #include <pybind11/stl_bind.h>

#include <array>
#include <iostream>
#include <memory>
#include <vector>

#include "connect.hpp"
#include "ic_master.hpp"
#include "i2c_master.hpp"

namespace py = pybind11;

auto bytes_info(py::bytes &bytes) {
  struct {
    const uint8_t *buff;
    size_t size;
  } res;
  py::buffer_info info(py::buffer(bytes).request());
  res.buff = reinterpret_cast<const uint8_t*>(info.ptr);
  res.size = static_cast<size_t>(info.size);
  return res;
}


const std::vector<uint8_t> to_vector(py::bytes &bytes) {
  auto [buff, size] = bytes_info(bytes);
  const std::vector<uint8_t> data(buff, buff + size);
  return data;
}

const py::bytes to_bytes(const std::vector<uint8_t> &data) {
  const char *buff = reinterpret_cast<const char*>(data.data());
  size_t size = data.size();
  return py::bytes(buff, size);
}



PYBIND11_MODULE(pyconnect, m) {
  m.doc() = "Python bindings for Netio, Netio-Next and FelixClient controllers";

  py::class_<Controller, std::shared_ptr<Controller>> controller(m, "Controller");
  controller
    .def_readwrite("reply", &Controller::reply)
    .def("connect", &Controller::connect)
    .def("disconnect", &Controller::disconnect)
    .def("send_data",
      [](Controller &self, py::bytes &bytes) {
        auto data = to_vector(bytes);
        self.send_data(data);
      }, py::arg("data"))
    .def("wait_reply", &Controller::wait_reply, py::arg("timeout") = 0);


  py::class_<NetioCtrl, std::shared_ptr<NetioCtrl>> netio_ctrl(m, "NetioCtrl", controller);
  netio_ctrl
    .def(py::init<const char*, uint16_t, netio_tag_t, uint16_t, netio_tag_t>(),
      py::arg("remotehost"),
      py::arg("rx_port"), py::arg("rx_tag"),
      py::arg("tx_port"), py::arg("tx_tag"));


  py::class_<NetioNextCtrl, std::shared_ptr<NetioNextCtrl>> nnext_ctrl(m, "NetioNextCtrl", controller);
  nnext_ctrl
    .def(py::init<const char*,
      const char*, uint16_t, netio_tag_t,
      const char*, uint16_t, netio_tag_t>(),
      py::arg("localhost"), 
      py::arg("rx_host"), py::arg("rx_port"), py::arg("rx_tag"),
      py::arg("tx_host"), py::arg("tx_port"), py::arg("tx_tag")
    );

  py::class_<FelixClientCtrl, std::shared_ptr<FelixClientCtrl>> fclient_ctrl(m, "FelixClientCtrl", controller);
  fclient_ctrl
    .def(py::init<std::string,
      std::string, std::string,
      netio_tag_t, netio_tag_t,
      int, bool>(),
      py::arg("localhost"),
      py::arg("bus_dir"), py::arg("bug_group"),
      py::arg("rx_fid"), py::arg("tx_fid"),
      py::arg("log_level"), py::arg("verbose_bus")
    );

  m.def("set_log_pattern",
    [](std::string pattern) {
      spdlog::apply_all([&](std::shared_ptr<spdlog::logger> logger) {
        logger->set_pattern(pattern);
      });
    }, py::arg("pattern"));

  m.def("set_log_level",
    [](uint level) {
      spdlog::apply_all([&](std::shared_ptr<spdlog::logger> logger) {
        logger->set_level(spdlog::level::level_enum(level));
      });
    }, py::arg("level"));
}
