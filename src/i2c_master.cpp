#include <stdint.h>
#include <thread>
#include <vector>
#include <assert.h>

#include "ic_master.hpp"
#include "i2c_master.hpp"

namespace {
  auto logger = make_log("i2c.master");
}


I2CMaster::I2CMaster(ICMaster &ic_mstr, uint16_t master) :
  ic(ic_mstr)
{
  num = master;
  BaseWrite = (ic.lpgbt_ver == 0 ? 0x0f0 : 0x100);
  BaseRead  = (ic.lpgbt_ver == 0 ? 0x15f : 0x16f);

  BaseWrite += num * 0x07;
  BaseRead  += num * 0x15;

  SLCDriveMode = 0;
  freq = Freq_200KHz;
}


void I2CMaster::reset() {
  logger->debug("I2C reset M{}", num);
  uint16_t RST0 = (ic.lpgbt_ver == 0 ? 0x12c : 0x13c);
  uint8_t reg = 0x04 >> num;
  ic.write(RST0, { 0x00 });
  ic.write(RST0, { reg }); // reset RSTi2cmx
  ic.write(RST0, { 0x00 });
}


std::vector<uint8_t> I2CMaster::read1(uint8_t i2c_addr, uint8_t mem_addr, uint8_t mem_size) {
  std::vector<uint8_t> data;

  ic.write(RegAddress, i2c_addr);

  ic.write(RegData0, getCR_nbyte(0));
  ic.write(RegCmd, I2C_WRITE_CR);

  ic.write(RegData0, mem_addr);
  ic.write(RegCmd, I2C_1BYTE_WRITE);
  waitTransaction();

  uint8_t nbyte = 16;
  if (nbyte <= mem_size) {
    ic.write(RegData0, getCR_nbyte(nbyte));
    ic.write(RegCmd, I2C_WRITE_CR);
  }

  while (mem_size > 0) {
    if (mem_size < nbyte) {
      nbyte = mem_size;
      ic.write(RegData0, getCR_nbyte(nbyte));
      ic.write(RegCmd, I2C_WRITE_CR);
    }
    mem_size -= nbyte;

    ic.write(RegCmd, I2C_READ_MULTI);
    waitTransaction();

    ic.read(RegRead15 - nbyte + 1, nbyte);
    auto reply = ic.wait_reply();
    auto res = ic.parse_reply(reply);

    assert(res.data.size() == nbyte);
    for (int k = 0; k < nbyte; k++) data.push_back(res.data[nbyte - k - 1]);
  }

  return data;
}


std::vector<uint8_t> I2CMaster::read2(uint8_t i2c_addr, uint16_t mem_addr, uint16_t mem_size) {
  std::vector<uint8_t> data;

  ic.write(RegAddress, i2c_addr);

  ic.write(RegData0, getCR_nbyte(2));
  ic.write(RegCmd, I2C_WRITE_CR);

  uint8_t addr_lo = mem_addr & 0xFF;
  uint8_t addr_hi = (mem_addr >> 8) & 0xFF;
  ic.write(RegData0, addr_lo, addr_hi);
  ic.write(RegCmd, I2C_W_MULTI_4BYTE0);

  ic.write(RegCmd, I2C_WRITE_MULTI);
  waitTransaction();

  uint8_t nbyte = 16;
  if (nbyte <= mem_size) {
    ic.write(RegData0, getCR_nbyte(nbyte));
    ic.write(RegCmd, I2C_WRITE_CR);
  }

  while (mem_size > 0) {
    if (mem_size < nbyte) {
      nbyte = mem_size;
      ic.write(RegData0, getCR_nbyte(nbyte));
      ic.write(RegCmd, I2C_WRITE_CR);
    }
    mem_size -= nbyte;

    ic.write(RegCmd, I2C_READ_MULTI);
    waitTransaction();

    ic.read(RegRead15 - nbyte + 1, nbyte);
    auto reply = ic.wait_reply();
    auto res = ic.parse_reply(reply);

    assert(res.data.size() == nbyte);
    for (int k = 0; k < nbyte; k++) data.push_back(res.data[nbyte - k - 1]);
  }

  return data;
}


void I2CMaster::write1(uint8_t i2c_addr, uint8_t addr, const std::vector<uint8_t> &data) {
  logger->trace("write1 | i2c_addr: {}, size: {}", i2c_addr, data.size());
  ic.write(RegAddress, i2c_addr);

  uint16_t size = data.size();
  uint16_t nbyte = 16;

  uint16_t pos = 0;
  if (pos + 15 <= size) {
    nbyte = 1 + 15;
    ic.write(RegData0, getCR_nbyte(nbyte));
    ic.write(RegCmd, I2C_WRITE_CR);
  }
  while (pos < size) {
    if (pos + 15 > size) {
      nbyte = 1 + size - pos;
      ic.write(RegData0, getCR_nbyte(nbyte));
      ic.write(RegCmd, I2C_WRITE_CR);
    }

    uint8_t val[16];
    val[0] = addr + pos;
    for (int k = 0; k < 15; k++) {
      val[1 + k] = (pos < size ? data[pos + k] : 0);
    }
    writeData(val, nbyte);
    ic.write(RegCmd, I2C_WRITE_MULTI);
    waitTransaction();
    pos += nbyte - 1;
  }
}


void I2CMaster::write2(uint8_t i2c_addr, uint16_t addr, const std::vector<uint8_t> &data) {
  logger->trace("write2 | i2c_addr: {}, size: {}", i2c_addr, data.size());
  ic.write(RegAddress, i2c_addr);

  uint16_t size = data.size();
  uint16_t nbyte = 16;

  uint16_t pos = 0;
  if (pos + 14 <= size) {
    nbyte = 2 + 14;
    ic.write(RegData0, getCR_nbyte(nbyte));
    ic.write(RegCmd, I2C_WRITE_CR);
  }
  while (pos < size) {
    if (pos + 14 > size) {
      nbyte = 2 + size - pos;
      ic.write(RegData0, getCR_nbyte(nbyte));
      ic.write(RegCmd, I2C_WRITE_CR);
    }

    uint8_t val[16];
    val[0] = (addr + pos) & 0xFF;
    val[1] = ((addr + pos) >> 8) & 0xFF;
    for (int k = 0; k < 14; k++) {
      val[2 + k] = (pos < size ? data[pos + k] : 0);
    }
    writeData(val, nbyte);
    ic.write(RegCmd, I2C_WRITE_MULTI);
    waitTransaction();
    pos += nbyte - 2;
  }
}


void I2CMaster::writeData(uint8_t val[16], uint8_t nbyte) {
  logger->trace("write_block | nbyte: {}", nbyte);

  if (nbyte > 0) {
    ic.write(RegData0, { val[0],  val[1],  val[2],  val[3] });
    ic.write(RegCmd, I2C_W_MULTI_4BYTE0);
  }
  if (nbyte > 4) {
    ic.write(RegData0, { val[4],  val[5],  val[6],  val[7] });
    ic.write(RegCmd, I2C_W_MULTI_4BYTE1);
  }
  if (nbyte > 8) {
    ic.write(RegData0, { val[8],  val[9],  val[10], val[11] });
    ic.write(RegCmd, I2C_W_MULTI_4BYTE2);
  }
  if (nbyte > 12) {
    ic.write(RegData0, { val[12], val[13], val[14], val[15] });
    ic.write(RegCmd, I2C_W_MULTI_4BYTE3);
  }
}


void I2CMaster::waitTransaction() {

  uint8_t status = 0;
  do {
    ic.read(RegStatus, 1);
    auto reply = ic.wait_reply();
    auto res = ic.parse_reply(reply);
    if (res.data.size() != 1) continue;
    status = res.data[0];

    if (status & 0x40) {
      logger->warn("The last transaction was not acknowledged by the I2C slave.");
    }
    if (status & 0x20) {
      logger->warn("Invalid command was sent to the I2C channel. Only cleared by a reset.");
    }
    if (status & 0x08) {
      logger->warn("SDA line is pulled low ‘0’ before initiating a transaction.");
    }
    if (status & 0x04) {
      logger->trace("The last I2C transaction was executed successfully.");
    }
    if (status && status != 0x04) {
      logger->trace("status = {x}", status);
      break;
    }
  } while (!(status & 0x04));
}
