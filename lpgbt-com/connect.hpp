#pragma once

#include <cstdint>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include "felixbase/client.hpp"

// netio
#include "netio/netio.hpp"

// netio-next
#include "netio/netio.h"
#include "netio/netio_tcp.h"

// felix client
#include "felix/felix_client.hpp"



std::shared_ptr<spdlog::logger> make_log(std::string logname);

class Controller {
protected:
  inline static auto logger = make_log("controller");

public:
  virtual ~Controller() = default;
  virtual void connect() = 0;
  virtual void disconnect() = 0;
  virtual void send_data(std::vector<uint8_t> &data) = 0;

  virtual bool wait_reply(uint32_t timeout = 5000) {
    uint sleep_ms = 10;
    uint time_ms = 0;
    while (time_ms < timeout) {
      if (!flag) return true;
      std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
      time_ms += sleep_ms;
    }
    flag = false;
    reply.clear();
    return false;
  }

  std::atomic<bool> flag;
  std::vector<uint8_t> reply;
};


// Netio
class NetioCtrl : public Controller {

public:
  NetioCtrl(std::string arg_remotehost,
    uint16_t rx_port, netio_tag_t rx_tag,
    uint16_t tx_port, netio_tag_t tx_tag)
  {
    logger->info("Netio controller:");
    logger->info("  tx_port: {:5d}, tx_tag: 0x{:x}", tx_port, tx_tag);
    logger->info("  rx_port: {:5d}, rx_tag: 0x{:x}", rx_port, rx_tag);
    remotehost = arg_remotehost;
    tx.port = tx_port; tx.tag = tx_tag;
    rx.port = rx_port; rx.tag = rx_tag;
    tx.sock = nullptr;
    rx.sock = nullptr;
    ctx = nullptr;
  };
  ~NetioCtrl() override {
    disconnect();
  }

  void connect() override;
  void disconnect() override;
  void send_data(std::vector<uint8_t> &data) override;

  struct {
    uint16_t port;
    netio_tag_t tag;
    netio::low_latency_subscribe_socket* sock;
    netio::endpoint ep;
  } rx;

  struct {
    uint16_t port;
    netio_tag_t tag;
    netio::low_latency_send_socket* sock;
    netio::endpoint ep;
  } tx;

  struct {
    std::string remotehost;

    uint16_t rx_port;
    netio_tag_t rx_tag;
    netio::low_latency_subscribe_socket* rx_sock;
    netio::endpoint rx_ep;

    uint16_t tx_port;
    netio_tag_t tx_tag;
    netio::low_latency_send_socket* tx_sock;
    netio::endpoint tx_ep;
  } rec;

  std::string remotehost;


private:
  netio::context* ctx;
  std::thread ctx_thread;

  void on_msg_recv(netio::endpoint& ep, netio::message& msg);
};


// Netio Next
class NetioNextCtrl : public Controller {

public:
  NetioNextCtrl(const char* localhost,
    const char* rx_host, uint16_t rx_port, netio_tag_t rx_tag,
    const char* tx_host, uint16_t tx_port, netio_tag_t tx_tag)
  {
    rx.localhost = localhost;
    rx.host = rx_host; rx.port = rx_port; rx.tag = rx_tag;
    tx.host = tx_host; tx.port = tx_port; tx.tag = tx_tag;
  };
  ~NetioNextCtrl() override {
    disconnect();
  }

  void connect() override;
  void disconnect() override;

  void send_data(std::vector<uint8_t> &buff) override;

  struct {
    std::string localhost;
    std::string host;
    uint16_t port;
    netio_tag_t tag;
    netio_context ctx;
    std::thread thread;
    netio_subscribe_socket socket;
  } rx;

  struct {
    std::string host;
    uint16_t port;
    netio_tag_t tag;
    netio_context ctx;
    std::thread thread;
    netio_buffered_send_socket socket;
  } tx;

  // Callbacks
  void rx_init();
  void tx_init();

  static void tx_on_init(void* ptr) {
    logger->debug("tx_on_init | ptr: 0x{:x}", reinterpret_cast<std::uintptr_t>(ptr));
    auto client = static_cast<NetioNextCtrl*>(ptr);
    client->tx_init();
  }

  static void rx_on_init(void* ptr) {
    logger->debug("rx_on_init | ptr: 0x{:x}", reinterpret_cast<std::uintptr_t>(ptr));
    auto client = static_cast<NetioNextCtrl*>(ptr);
    client->rx_init();
  }

  static void tx_connection_established(struct netio_buffered_send_socket* socket) {
    logger->debug("tx connection established | socket.usr: 0x{:x}",
                  reinterpret_cast<std::uintptr_t>(socket->usr));
  }

  static void tx_connection_closed(struct netio_buffered_send_socket* socket) {
    logger->debug("tx on_connection_closed");
  }

  static void tx_error_connection_refused(struct netio_buffered_send_socket* socket) {
    logger->debug("tx on_error_connection_refused");
  }

  static void rx_connection_closed(struct netio_subscribe_socket* socket) {
    logger->debug("rx connection to publisher closed from {}:{}",
                  socket->remote_hostname, socket->remote_port);
  }

  static void rx_connection_established(struct netio_subscribe_socket* socket) {
    logger->debug("rx connection established to {}:{}, socket.usr: 0x{:x}",
                  socket->remote_hostname, socket->remote_port,
                  reinterpret_cast<std::uintptr_t>(socket->usr));
  }

  static void rx_error_connection_refused(struct netio_subscribe_socket* socket) {
    logger->debug("rx connection refused for subscribing from {}:{}",
                  socket->remote_hostname, socket->remote_port);
  }

  static void on_msg_received(struct netio_subscribe_socket* socket,
    netio_tag_t tag, void* data, size_t size);

};



// FelixClient

class FelixClientCtrl : public Controller {

public:
  FelixClientCtrl(std::string localhost,
    std::string bus_dir, std::string bus_group,
    netio_tag_t rx_fid, netio_tag_t tx_fid, int log_level = 0, bool verbose_bus = false)
  {
    // trace, debug, info, notice, warning, error, fatal
    //auto log_level = LogLevel(log_level);
    //bool verbose_bus = true;
    unsigned netio_pages = 0;
    unsigned netio_pagesize = 0;

    client = new FelixClient(localhost, bus_dir, bus_group,
    log_level, verbose_bus, netio_pages, netio_pagesize);

    client->callback_on_data(std::bind(&FelixClientCtrl::on_msg_recv, this,
      std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));

    // client->callback_on_user_timer(std::bind(&Controller::on_user_timer, this));
    // client->user_timer_init();
    // client->user_timer_start(100);

    thread = std::make_unique<std::thread>(&FelixClient::run, client);

    this->rx_fid = rx_fid;
    this->tx_fid = tx_fid;
  }

  ~FelixClientCtrl() override {
    if (!client) return;

    //client->user_timer_stop();
    client->stop();
    if (thread->joinable()) thread->join();
    delete client;
  };

  void connect() override {
    client->init_send_data(tx_fid);
    client->init_subscribe(rx_fid);
    client->subscribe(rx_fid);
  }
  void disconnect() override {
    client->unsubscribe(rx_fid);
  }

  void send_data(std::vector<uint8_t> &data) override {
    flag = true;
    logger->trace("send_data | fid: 0x{:x}, size: {}", tx_fid, data.size());
    client->send_data(tx_fid, data.data(), data.size(), true);
  }

  void on_msg_recv(netio_tag_t fid, const uint8_t* data, size_t size, uint8_t status) {
    logger->trace("msg_recv | size: {}", size);
    if (size == 0) return;

    //printf("msg_recv:");
    //for(uint i = 0; i < size; i++) {
    //  if (i % 8 == 0) printf(" ");
    //  printf("%02x", data[i]);
    //}
    //printf("\n");

    reply.assign(data, data + size);
    flag = false;
  }

  netio_tag_t rx_fid;
  netio_tag_t tx_fid;

  FelixClient* client = nullptr;
  std::unique_ptr<std::thread> thread;
};
