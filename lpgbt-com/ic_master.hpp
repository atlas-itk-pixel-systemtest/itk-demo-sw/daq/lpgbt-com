#pragma once

#include <fstream>
#include <sstream>
#include <iostream>
#include <bitset>
#include <vector>

#include "connect.hpp"

void print_hex(std::vector<uint8_t> data);
void print_bin(std::vector<uint8_t> data);


struct ICReply {
  uint16_t i2c_addr;
  uint16_t mem_addr;
  uint16_t mem_size;
  uint8_t  command;
  uint8_t  parity;
  uint8_t  status;
  std::vector<uint8_t> data;
};


class ICMaster {

public:

  ICMaster();
  ICMaster(Controller &ctrl);
  ~ICMaster();

  void send_write(uint16_t i2c_addr, uint16_t mem_addr, const uint8_t *data, uint16_t size);

  void send_read(uint16_t i2c_addr, uint16_t mem_addr, uint16_t size);

  struct ICReply send(uint16_t mem_addr, uint16_t mem_size, const uint8_t *data = nullptr);

  std::vector<uint8_t> read(uint16_t addr, uint16_t size) {
    auto reply = send(addr, size);
    return reply.data;
  }
  void write(uint16_t addr, const uint8_t *data, uint16_t size) {
    send(addr, size, data);
  }
  void write(uint16_t addr, const std::vector<uint8_t> &vec) {
    send(addr, vec.size(), vec.data());
  }
  void write(uint16_t addr, uint8_t data) {
    std::vector<uint8_t> vec = { data };
    write(addr, vec);
  }
  void write(uint16_t addr, uint8_t data0, uint8_t data1) {
    std::vector<uint8_t> vec = { data0, data1 };
    write(addr, vec);
  }
  std::vector<uint8_t> wait_reply(uint timeout = 0) {
    if (!timeout) timeout = reply_timeout;
    ctrl->wait_reply();
    return ctrl->reply;
  }
  void print_reply(std::vector<uint8_t> &msg);
  struct ICReply parse_reply(std::vector<uint8_t> &msg);


  Controller *ctrl = nullptr;
  uint16_t i2c_addr;
  uint8_t lpgbt_ver;

  uint reply_timeout  = 5000;
  uint resend_timeout = 1000;
};
