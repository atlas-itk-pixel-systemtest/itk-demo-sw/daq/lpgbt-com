# FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
# FROM cern/cc7-base
FROM centos:7

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN yum -y install dnf
# && yum -y clean all
RUN dnf -y install \
    libfabric-devel \
    tbb-devel \
    libuuid-devel \
    python3-devel && \
    dnf -y group install "Development Tools" && \
    dnf -y clean all

RUN dnf -y install centos-release-scl-rh
RUN dnf -y install devtoolset-11

# python dependencies
RUN dnf -y install openssl-devel

# full python dependencies
# RUN yum install –y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel gdbm-devel ncurses-devel db4-devel wget

ENV HOME=/root
WORKDIR ${HOME}

RUN git clone https://github.com/pyenv/pyenv.git pyenv
COPY ./setup_pyenv.sh .
ENV PYENV_ROOT=${HOME}/pyenv
ENV PATH="$PYENV_ROOT/bin:$PATH"
RUN git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
RUN . ./setup_pyenv.sh && \
    pyenv install 3.9.16 


# RUN git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/daq/lpgbt-com.git
# for CI use local directory
COPY . lpgbt-com/

RUN mkdir -p lpgbt-com/cmake && \
    curl -sSLo lpgbt-com/cmake/CPM.cmake https://github.com/cpm-cmake/CPM.cmake/releases/latest/download/CPM.cmake

RUN python3 -m pip install --upgrade pip

RUN . /opt/rh/devtoolset-11/enable && \
    . ./setup_pyenv.sh && \
    pyenv virtualenv 3.9.16 .venv && \
    pyenv activate .venv && \
    python3 -m pip install nanobind cmake && \
    python3 -m pip -v install ./lpgbt-com

