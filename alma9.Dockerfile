FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN dnf --enablerepo=crb install -y \
    libfabric-devel \
    tbb-devel
RUN dnf -y install python3-devel
RUN dnf -y group install "Development Tools"
RUN dnf -y install libuuid-devel

RUN python3 -m ensurepip
RUN python3 -m pip install --upgrade pip

WORKDIR /root

RUN git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/daq/lpgbt-com.git
# COPY . .

WORKDIR /root/lpgbt-com
RUN mkdir -p cmake && \
    curl -sSLo cmake/CPM.cmake https://github.com/cpm-cmake/CPM.cmake/releases/latest/download/CPM.cmake

RUN python3 -m pip install .
